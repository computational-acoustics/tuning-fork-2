using FEATools


function make_animation(
    source_file::String,
    destination_file::String,
    number_of_periods::Integer,
    frames_per_period::Integer
    )
    
    source_data = load_mesh(source_file)
    destination_data = load_mesh(source_file)
    
    n_frames = frames_per_period * number_of_periods
    l_pad_size = ceil(Int, log10(n_frames))
    
    for f in 0:(n_frames - 1)
    
        destination_dict = source_data.point_data
        
        pressure_field = 
            destination_dict["pressure wave 1"] .+ 
            im .* destination_dict["pressure wave 2"]
            
        instantaneous_pressure = 
            abs.(pressure_field) .* 
            cos.(angle.(pressure_field) .+ (2π * f / frames_per_period))
            
        instantaneous_displacement = 
            destination_dict["displacement HarmonicMode1"] .*
            cospi(2 * f / frames_per_period)
            
            
        destination_dict["pressure"] = instantaneous_pressure
        destination_dict["displacement"] = instantaneous_displacement
            
        delete!(destination_dict, "pressure wave 1")
        delete!(destination_dict, "pressure wave 2")
        delete!(destination_dict, "displacement HarmonicMode1")
        
        destination_data.point_data = destination_dict
        
        write_mesh(
            destination_file * "_t" * lpad(f, l_pad_size, "0") * ".vtu",
            destination_data
        )

    end
    
end

make_animation(
    "elmerfem/case_t0001.vtu", 
    "animation/case", 
    1, 
    24 * 2
)

