# Tuning Fork 2

ElmerFEM Model of a Tuning Fork coupled to air.

Part of the [accompanying repositories](https://gitlab.com/computational-acoustics) of the [Computational Acoustics with Open Source Software](https://computational-acoustics.gitlab.io/website/) project.

## Covering Episodes

* [Tuning Fork - Part 3](https://computational-acoustics.gitlab.io/website/posts/27-tuning-fork-part-3/).

## Study Summary

The main parameters of the study are reported below.

### Domain

$`512`$ $`text{Hz}`$ Tuning Fork by [Justin Black](http://justinablack.com/tuning_fork_freqs/) immersed in a body of air.

| Body        | Size                                      | Mesh Algorithm  | Mesh Min. Size        | Mesh Max. Size        | Element Order           |
|-------------|-------------------------------------------|-----------------|-----------------------|-----------------------|-------------------------|
| Tuning Fork | $`164`$ X $`25`$ X $`9.56`$ $`\text{mm}`$ | NETGEN 1D-2D-3D | $`1`$ $`\text{mm}`$   | $`1.5`$ $`\text{mm}`$ | Second ($`p`$-elements) |
| Air         | $`250`$ $`\text{mm}`$ radius sphere       | NETGEN 1D-2D-3D | $`1`$ $`\text{mm}`$   | $`15`$ $`\text{mm}`$  | Second ($`p`$-elements) |



### Material

## Fork

Aluminum 6061

| Property        | Value                                     |
|-----------------|-------------------------------------------|
| Density         | $`2700`$ $`\frac{\text{kg}}{\text{m}^3}`$ |
| Young's Modulus | $`70 \cdot 10^{9}`$ $`\text{Pa}`$         |
| Poisson's Ratio | $`0.35`$                                  |

## Air

Air at room temperature.

| Property       | Value                                      |
|----------------|--------------------------------------------|
| Density        | $`1.205`$ $`\frac{\text{kg}}{\text{m}^3}`$ |
| Speed of Sound | $`343`$ $`\frac{\text{m}}{\text{s}}`$      |

### Boundary Conditions

## Fork

Simply supported at the bottom of its handle.

## Air

Displacement wake from free fork boundaries, plane wave outlet at the exterior boundary.

## Software Overview

The table below reports the software used for this project.

| Software                                           | Usage                        | Version |
|----------------------------------------------------|------------------------------|---------|
| [FreeCAD](https://www.freecadweb.org/)             | 3D Modeller                  | 0.19    |
| [Salome Platform](https://www.salome-platform.org) | Pre-processing               | 9.6.0   |
| [ElmerFEM](http://www.elmerfem.org)                | Multiphysical solver         | 9.0     |
| [ParaView](https://www.paraview.org/)              | Post-processing              | 5.9.0   |
| [Julia](https://julialang.org/)                    | Technical Computing Language | 1.5.3   |

## Repo Structure

* `elmerfem` contains the Elmer project.
* `geometry.FCStd` is the FreeCAD geometry model. This file can be used to export geometric entities to _BREP_ files to pre-process with Salome. _BREP_ files are excluded from the repo as they are redundant.
* `study.hdf` is the Salome study of the geometry. It contains the geometry pre-processing and meshing. Note that the mesh in this file is **not** computed.
* The `*.pvsm` files are ParaView state files. They can be loaded into ParaView to plot the solution.
* `camera.pvcc` contains camera settings for ParaView.
* The `make_animation.jl` file contains Julia code to postprocess the Elmer results. It will produce a timestampend `vtu` series which animates the system motion. The results are stored in the `animation` folder.
* The `run.sh` script allows to run the study and produce the animation, provided that your Julia environment has the required packages (see below).

The repo contains only the _source_ of the simulation. To obtain results, the study must be solved.

## How to Run this Study

Follow the steps below to run the study.

Clone the repository and `cd` into the cloned directory:

```bash
git clone https://gitlab.com/computational-acoustics/tuning-fork-2.git
cd tuning-fork-2/
```

`cd` in the `elmerfem` directory and run the study:

```bash
cd elmerfem/
ElmerSolver
```

When finished, open ParaView and select `File > Load State` to load the `.pvsm` files in the root of the repository. To load the ParaView state successfully, choose _Search files under specified directory_ as shown in the example below. The folder specified in `Data Directory` should be the root folder of the repo (blurred below as the absolute path will differ in your machine).

![Load State Example](res/pictures/paraview-load-state.png)

To run `make_animation.jl` your julia environment needs the [FEATools](https://gitlab.com/computational-acoustics/featools.jl) package. After this package is installed in your Julia environment return to the repository root and issue this command:

```bash
cd ..
julia make_animation.jl
```

The staps above are automatised by the `run.sh` bash script.

